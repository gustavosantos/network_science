from graph_tool.all import *
import numpy as np

def get_stats(input_data):
    print('Minimum:', np.min(input_data))
    print('Maximum:', np.max(input_data))
    print('Mean:', np.mean(input_data))
    print('Median:', np.median(input_data))
    print('95th percentile:', np.percentile(input_data, 95))
    print('99th percentile:', np.percentile(input_data, 99))
    print('Standard deviation:', np.std(input_data))

def plot_distribution(input_data):
    plt.hist(input_data, bins=range(int(max(input_data)) + 1))
    plt.semilogy()
    plt.show()

def get_general_stats(input_graph):
    print('General stats')
    nvert = input_graph.num_vertices()
    nedge = input_graph.num_edges()
    components, hist = label_components(input_graph, directed=True)
    print('Number of vertices:', nvert)
    print('Number of edges:', nedge)
    print('Number of strongly connected components', len(hist))

def sample_distances(input_graph, sample_fraction):
    sample_size = int(input_graph.num_vertices() * sample_fraction)
    print('Sample fraction:', sample_fraction)
    print('Number of sampled vertices:', sample_size)
    sampled_indexes = np.random.choice(input_graph.get_vertices(),
                                        size=sample_size)
    sampled_vertices = input_graph.get_vertices()[sampled_indexes]

    all_distances = np.array([])
    for i in range(len(sampled_vertices)):
        curr_source = sampled_vertices[i]
        curr_dests = sampled_vertices[sampled_vertices != curr_source]

        curr_distances = shortest_distance(input_graph, source=curr_source,
                                           target=curr_dests, directed=True)
        #Consider only reachable destinations
        curr_distances = curr_distances[curr_distances != 2147483647]
        all_distances = np.concatenate((all_distances, curr_distances))
    return all_distances

def small_word_stats(input_graph, sample_fraction):
    print('\nSmall world')
    nvert = input_graph.num_vertices()
    nedge = input_graph.num_edges()
    print('Number of vertices:', nvert)
    print('Number of edges:', nedge)
    distances = sample_distances(input_graph, sample_fraction)
    print('Distances:')
    get_stats(distances)
    print('Pseudo diameter:', pseudo_diameter(input_graph)[0])
    return distances

def friends_are_friends_stats(input_graph):
    print('\nFriends are friends')
    nvert = input_graph.num_vertices()
    nedge = input_graph.num_edges()
    clust = local_clustering(input_graph, undirected=False)
    print('Local clustering average:', vertex_average(input_graph, clust))
    print('Global clustering:', global_clustering(input_graph))
    print('Density:', (2 * nedge) / (nvert * (nvert - 1)))

def no_normality_stats(input_graph):
    print('\nNo normality')
    degrees = input_graph.get_out_degrees(input_graph.get_vertices())
    print('Degree (out):')
    get_stats(degrees)
    degrees = input_graph.get_in_degrees(input_graph.get_vertices())
    print('Degree (in):')
    get_stats(degrees)

def all_connected_stats(input_graph):
    print('\nAll connected')
    nvert = input_graph.num_vertices()
    components, hist = label_components(input_graph, directed=True)
    ncomponents = len(hist)
    print('Number of vertices:', nvert)
    print('Number of components:', ncomponents)
    print('Components:')
    get_stats(hist)
    print('Size of largest strongly connected component:',
          extract_largest_component(input_graph).num_vertices())
