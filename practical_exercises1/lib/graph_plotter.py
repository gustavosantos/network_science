import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from statsmodels.distributions.empirical_distribution import ECDF
from graph_tool.all import label_components

def plot_eccdf(value_list, output_filename, is_loglog=True):
    _, ax = plt.subplots(nrows=1, figsize=(15, 10))

    x1 = np.linspace(min(value_list), max(value_list), len(value_list))
    y1 = 1 - ECDF(value_list)(x1)
    if is_loglog:
        plt.plot(x1, y1, 'x', color='r')
    else:
        plt.plot(x1, y1, '-x', color='r')

    plt.yticks(np.arange(0, 1.05, 0.05))
    plt.grid(True)
    if is_loglog:
        plt.loglog()
    plt.savefig(output_filename)

def plot_degree_eccdf(input_graph, output_dir, output_filename):
    degrees = input_graph.get_in_degrees(input_graph.get_vertices())
    plot_eccdf(degrees, output_dir + 'in_' + output_filename)
    degrees = input_graph.get_out_degrees(input_graph.get_vertices())
    plot_eccdf(degrees, output_dir + 'out_' + output_filename)

def plot_components_eccdf(input_graph, output_dir, output_filename):
    components, hist = label_components(input_graph, directed=True)
    plot_eccdf(hist, output_dir + output_filename, is_loglog=False)
    plot_eccdf(hist, output_dir + 'log_log' + output_filename, is_loglog=True)

def plot_distance_eccdf(input_graph, distances, output_dir, output_filename):
    plot_eccdf(distances, output_dir + output_filename, is_loglog=False)
    plot_eccdf(distances, output_dir + 'loglog_'+output_filename, is_loglog=True)