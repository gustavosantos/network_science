from graph_tool.all import load_graph_from_csv
from lib.graph_processor import *
from lib.graph_plotter import *

def print_stats(input_graph, sample_fraction=0.01):
    get_general_stats(input_graph)
    distances = small_word_stats(input_graph, sample_fraction)
    friends_are_friends_stats(input_graph)
    no_normality_stats(input_graph)
    all_connected_stats(input_graph)
    return distances

def plot_distributions(input_graph, distances, dataset_id):
    plot_components_eccdf(input_graph, 'output/', 'components_'+dataset_id+'.png')
    plot_degree_eccdf(input_graph, 'output/', 'degree_'+dataset_id+'.png')
    plot_distance_eccdf(input_graph, distances, 'output/',
                        'distances_'+dataset_id+'.png')

def main():
    """Main function"""
    data_path = 'input/'

    print('_____________________________________________________________________')
    print('Twitter')
    input_graph = graph_tool.load_graph_from_csv(data_path+'twitter_combined.txt',
                                                 skip_first=False, directed=True,
                                                 csv_options={'delimiter': ' '})
    sample_fraction = 0.01
    distances = print_stats(input_graph, sample_fraction)
    plot_distributions(input_graph, distances, 'twitter')

    print('_____________________________________________________________________')
    print('\n')
    print('_____________________________________________________________________')
    print('Web')
    input_graph = graph_tool.load_graph_from_csv(data_path+'web-Google.txt',
                                                 skip_first=False, directed=True,
                                                 csv_options={'delimiter': '\t'})
    sample_fraction = 0.001
    distances = print_stats(input_graph, sample_fraction)
    plot_distributions(input_graph, distances, 'web')

    print('_____________________________________________________________________')
    print('\n')
    print('_____________________________________________________________________')
    print('Academic collaboration')
    input_graph = graph_tool.load_graph_from_csv(data_path+'cit-HepPh.txt',
                                                 skip_first=False, directed=True,
                                                 csv_options={'delimiter': '\t'})
    sample_fraction = 0.05
    distances = print_stats(input_graph, sample_fraction)
    plot_distributions(input_graph, distances, 'colab')

    print('_____________________________________________________________________')
    print('\n')
    print('_____________________________________________________________________')
    print('Google+')
    input_graph = graph_tool.load_graph_from_csv(data_path+'gplus_combined.txt',
                                                 skip_first=False, directed=True, 
                                                 csv_options={'delimiter': ' '})
    sample_fraction = 0.01
    distances = print_stats(input_graph, sample_fraction)
    plot_distributions(input_graph, distances, 'googleplus')
    print('_____________________________________________________________________')

if __name__ == '__main__':
    main()
